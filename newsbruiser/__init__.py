from functools import wraps
import os
import random
import string

import flask
from flask import (
    abort,
    Flask,
    render_template,
    session,
)

from newsbruiser.model import (
    production_session,
    get_one,
    Blog,
    Tag,
)

from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.secret_key = "".join(random.choice(string.printable) for i in range(32))
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, '../newsbruiser.sqlite')
db = SQLAlchemy(app)
_db = db.session

def blog_route(path, *args, **kwargs):
    """Decorator to create routes that start with a blog short_name in their
    url path prefix. To be used with @has_blog.
    """
    def decorator(f):
        # This sets up routes for both the subdomain and the url path prefix.
        # The order of these determines which one will be used by url_for -
        # in this case it's the prefix route.
        # We may want to have a configuration option to specify whether to
        # use a subdomain or a url path prefix.
        for suffix in ('', '/'):
            prefix_route = app.route(
                "/<blog>/" + path + suffix, *args, **kwargs
            )(f)
            default_blog_route = app.route(
                "/" + path + suffix, *args, **kwargs
            )(f)
        return default_blog_route
    return decorator

def has_blog(f):
    """Decorator to figure out which blog is requested and look it up."""
    @wraps(f)
    def decorated(*args, **kwargs):
        blog = kwargs.pop("blog", None)
        if blog:
            blog = get_one(_db, Blog, short_name=blog)
        else:
            blog = get_one(_db, Blog, default=True)
        if not blog:
            abort(404)
        flask.request.blog = blog
        return f(*args, **kwargs)
    return decorated


@blog_route("login", methods=["GET", "POST"])
@has_blog
def login():
    blog = flask.request.blog
    if 'password' in flask.request.form:
        password = flask.request.form['password']
        if blog.password_match(password):
            session['authorized'] = True
        else:
            session['authorized'] = False
    return render_template(
        "login.html", title="Log in", blog=blog,
        authorized=session.get('authorized', False)
    )

@blog_route("")
@has_blog
def posts():
    blog = flask.request.blog
    return render_template("posts.html", title=blog.name, blog=blog.short_name, posts=blog.posts[-2:])

@blog_route("<path:identifier>")
@has_blog
def permalink(identifier):
    blog = flask.request.blog
    title, posts = blog.lookup(identifier)
    return render_template("posts.html", blog=blog.short_name, title=title, posts=posts)

@blog_route("post/<id>")
@has_blog
def internal_permalink(id):
    blog = flask.request.blog
    post = blog.lookup_by_id(identifier)
    return render_template("posts.html", blog=blog.short_name, title=post.title_or_excerpt, posts=[post])


@blog_route("tag/<tag>")
@has_blog
def posts_for_tag(tag):
    blog = flask.request.blog
    tag = get_one(_db, Tag, blog=blog, name=tag)
    return render_template("posts.html", title=tag.name, blog=blog.short_name, posts=tag.posts)


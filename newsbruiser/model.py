import bcrypt
import datetime
import logging
import pytz
import re
from bs4 import BeautifulSoup
from sqlalchemy import (
    create_engine,
    LargeBinary,
    Boolean,
    Column,
    Enum,
    Float,
    Integer,
    Unicode,
    DateTime,
    ForeignKey,
    Table,
    UniqueConstraint,
)
from sqlalchemy.exc import (
    IntegrityError
)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import (
    backref,
    relationship,
)
from sqlalchemy.orm.exc import (
    NoResultFound,
    MultipleResultsFound,
)
from sqlalchemy.orm.session import Session
from sqlalchemy.ext.declarative import declarative_base

TIME_FORMAT = "%Y-%m-%d %H:%M"

def _now():
    """The current time."""
    return datetime.datetime.now(tz=pytz.UTC)

Base = declarative_base()

def create(db, model, create_method='',
           create_method_kwargs=None,
           **kwargs):
    """Create a single model object."""
    kwargs.update(create_method_kwargs or {})
    created = getattr(model, create_method, model)(**kwargs)
    db.add(created)
    db.flush()
    return created, True

def get_one(db, model, on_multiple='error', **kwargs):
    """Gets an object from the database based on its attributes.

    :return: object or None
    """
    q = db.query(model).filter_by(**kwargs)
    try:
        return q.one()
    except MultipleResultsFound as e:
        if on_multiple == 'error':
            raise e
        elif on_multiple == 'interchangeable':
            # These records are interchangeable so we can use
            # whichever one we want.
            #
            # This may be a sign of a problem somewhere else. A
            # database-level constraint might be useful.
            logging.warn("Picking interchangeable records for %s" % q)
            q = q.limit(1)
            return q.one()
    except NoResultFound:
        return None

def get_one_or_create(db, model, create_method='',
                      create_method_kwargs=None,
                      **kwargs):
    """Get a single model object. If it doesn't exist, create it."""
    one = get_one(db, model, **kwargs)
    if one:
        return one, False
    else:
        try:
            # These kwargs are supported by get_one() but not by create().
            get_one_keys = ['on_multiple', 'constraint']
            for key in get_one_keys:
                if key in kwargs:
                    del kwargs[key]
            obj = create(db, model, create_method, create_method_kwargs, **kwargs)
            return obj
        except IntegrityError as e:
            logging.info(
                "INTEGRITY ERROR on %r %r, %r: %r", model, create_method_kwargs, 
                kwargs, e)
            return db.query(model).filter_by(**kwargs).one(), False


def engine(filename):
    """Return an engine for and database connection to the
    SQLite database at `filename`.
    """
    engine = create_engine('sqlite:///%s' % filename, echo=False)
    Base.metadata.create_all(engine)
    return engine, engine.connect()    
        
def production_session(filename):
    """Get a database connection to the SQLite database at `filename`."""
    e, connection = engine(filename)
    session = Session(connection)
    return session


class Blog(Base):
    __tablename__ = 'blogs'

    id = Column(Integer, primary_key=True)

    # The human-readable name of the blog.
    name = Column(Unicode)

    # One blog can be designated the default -- entering one of the
    # controllers without naming a blog will name this one.
    default = Column(Boolean, default=False)
    
    # A short name for the blog, used in URLs.
    short_name = Column(Unicode, unique=True, index=True)

    # A human-readable description for the blog.
    description = Column(Unicode)

    # A human-readable string describing the blog's author.
    author_name = Column(Unicode)
    author_email = Column(Unicode)

    # An image for use in the Atom feed.
    image = Column(LargeBinary)

    # Time zone of the person whose blog this is.
    timezone = Column(Unicode)

    # Admin password for the blog.
    password_hashed = Column(Unicode)
    
    posts = relationship("Post", backref="blog")
    tags = relationship("Tag", backref="blog")

    spaces_re = re.compile("\s+")
    slug_re = re.compile("[^a-z ]")

    year_re = re.compile("^([0-9]+)$")
    month_re = re.compile("^([0-9]+)/([0-9]+)$")
    day_re = re.compile("^([0-9]+)/([0-9]+)/([0-9]+)$")

    @property
    def timezone_obj(self):
        return pytz.timezone(self.timezone)

    @hybrid_property
    def password(self):
        raise NotImplementedError("Password cannot be read, only compared.")

    @password.setter
    def password(self, value):
        if isinstance(value, str):
            value = value.encode("utf8")
        self.password_hashed = bcrypt.hashpw(value, bcrypt.gensalt()).decode("ascii")

    def password_match(self, value):
        if isinstance(value, str):
            value = value.encode("utf8")
        return self.password_hashed == str(bcrypt.hashpw(
            value, self.password_hashed.encode("ascii")
        ))
        
    def lookup_by_id(self, id):
        _db = Session.object_session(self)
        try:
            id = int(id)
        except ValueError:
            return None

        post = get_one(_db, Post, blog=self, id=identifier_id)
        if post:
            return post
        return None
        
    def lookup(self, identifier, chronological=True):
        """Look one or more existing posts.
        
        :return: A 2-tuple (page title, [Post]), or None
           if the identifier doesn't make sense.
        """
        _db = Session.object_session(self)
        
        # Legacy slug
        post = get_one(_db, Post, blog=self, legacy_slug=identifier)
        if post is not None:
            return post.title_or_excerpt, [post]
        
        # Date slug + title slug
        if '/' in identifier:        
            date_slug, title_slug = identifier.rsplit('/', 1)
            post = get_one(
                _db, Post, blog=self, date_slug=date_slug,
                title_slug=title_slug
            )
            if post:
                return post.title_or_excerpt, [post]

        # If this doesn't match any specific entry, it might be an
        # indicator of a span of time such as a year or month.
        start = end = None
        y = self.year_re.match(identifier)
        tz = self.timezone_obj
        if y is not None:
            year = int(y.groups()[0])
            start = datetime.datetime(year, 1, 1, tzinfo=tz)
            end = datetime.datetime(year+1, 1, 1, tzinfo=tz)
            title = "{blog} for {year}".format(
                blog=self.name, year=year
            )

        if start is None:
            ym = self.month_re.match(identifier)
            if ym is not None:
                year, month = [int(x) for x in ym.groups()]
                start = datetime.datetime(year, month, 1, tzinfo=tz)
                if month == 12:
                    year += 1
                    month = 0
                end = datetime.datetime(year, month+1, 1, tzinfo=tz)
                title = "{blog} for {date}".format(
                    blog=self.name, date=start.strftime("%B %Y")
                )

        if start is None:
            ymd = self.day_re.match(identifier)
            if ymd is not None:
                year, month, day = [int(x) for x in ymd.groups()]
                start = datetime.datetime(year, month, day, tzinfo=tz)
                end = start + datetime.timedelta(days=1)
                title = "{blog} for {date}".format(
                    blog=self.name, date=start.strftime("%B %d, %Y")
                )
        if start is None:
            return None
        posts = _db.query(Post).filter(Post.blog==self).filter(
            Post.created>=start).filter(Post.created < end)
        if chronological:
            order = Post.created.asc()
        else:
            order = Post.created.desc()
        posts = posts.order_by(order)
        return title, posts
    
    def calculate_slugs(self, dt, title, content):
        _db = Session.object_session(self)

        date_slug = dt.strftime("%Y/%m")
        if not title or len(title) > 30:
            title_slug = Post._excerpt(title or content, include_ellipses=False)
        else:
            title_slug = title
        title_slug = self.slug_re.sub("", title_slug.strip().lower())
        title_slug = self.spaces_re.sub(" ", title_slug)
        title_slug = title_slug.strip().replace(" ", "-")

        extra = None
        match = True
        the_slug = None
        while match is not None:
            if extra is not None:
                if title_slug:
                    the_slug = title_slug + '-' + str(extra)
                else:
                    title_slug = '0'
                    the_slug = title_slug + str(extra)
            else:
                the_slug = title_slug
            match = get_one(
                _db, Post, blog=self, date_slug=date_slug,
                title_slug=the_slug
            )
            if match is not None:
                # Already a post with these slugs.
                if extra is None:
                    extra = 1
                else:
                    extra += 1
        # print("!", date_slug, the_slug)
        return date_slug, the_slug


class Post(Base):
    __tablename__ = 'posts'
    id = Column(Integer, primary_key=True)
    blog_id = Column(
        Integer, ForeignKey('blogs.id'), index=True, nullable=False
    )
    
    # The time the post was originally created.
    created = Column(DateTime(timezone=True), index=True)

    # The time the post was last modified.
    modified = Column(DateTime(timezone=True))

    title = Column(Unicode)

    date_slug = Column(Unicode, nullable=False, index=True)
    title_slug = Column(Unicode, index=True)
    legacy_slug = Column(Unicode, index=True)

    content = Column(Unicode, index=True)
    is_draft = Column(Boolean, default=False)
    
    comments = relationship("Comment", backref="post")

    __table_args__ = (
        UniqueConstraint('blog_id', 'date_slug', 'title_slug'),
    )

    @classmethod
    def _excerpt(cls, s, include_ellipses=False):
        ellipses = '…'
        excerpt = BeautifulSoup(s[:100], 'html.parser').get_text()
        excerpt = excerpt.strip()
        excerpt = excerpt[:30]
        if not excerpt:
            if include_ellipses:
                return ellipses
            else:
                return ''
        if len(excerpt) <= 30:
            return excerpt
        if ' ' in text:
            excerpt = excerpt[excerpt.rindex(' ')]
        if include_ellipses:
            return excerpt + ellipses
            
    @property
    def title_or_excerpt(self):
        if self.title:
            return self.title        
        return self._excerpt(self.content, include_ellipses=True)
        
class Comment(Base):
    __tablename__ = 'comments'
    id = Column(Integer, primary_key=True)
    post_id = Column(
        Integer, ForeignKey('posts.id'), index=True, nullable=False,
    )

    # The name of the comment's author.
    author = Column(Unicode, index=True)

    # The content of the comment.
    content = Column(Unicode)
    
    # The time the comment was created.
    created = Column(DateTime(timezone=True), index=True)

    # The IP addess that originated the comment
    ip_address = Column(Unicode, index=True)

    # Moderation status - enum of yes, no, unknown
    moderation_status = Column(
        Enum("accepted", "rejected", "pending"),
        default="pending", index=True
    )
    
    # Spamminess
    spamminess = Column(Float)

    
class Tag(Base):
    """A tag for a post."""

    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True)
    blog_id = Column(
        Integer, ForeignKey('blogs.id'), index=True, nullable=False
    )
    name = Column(Unicode)

    __table_args__ = (
        UniqueConstraint('blog_id', 'name'),
    )

    posts = relationship(
        "Post", secondary=lambda: classifications, backref="tags"
    )


classifications = Table(
    'classifications', Base.metadata,
    Column(
        'post_id', Integer, ForeignKey('posts.id'),
        index=True, nullable=False
    ),
    Column(
        'tag_id', Integer, ForeignKey('tags.id'),
        index=True, nullable=False
    ),
    UniqueConstraint('post_id', 'tag_id'),
)
